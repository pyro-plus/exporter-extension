<?php namespace Defr\ExporterExtension;

use Anomaly\Streams\Platform\Addon\Extension\Extension;

/**
 * Extension class.
 *
 * @package defr.extension.exporter
 *
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 */
class ExporterExtension extends Extension
{

}
