<?php namespace Defr\ExporterExtension\Http\Controller\Admin;

use Anomaly\Streams\Platform\Field\Contract\FieldInterface;
use Anomaly\Streams\Platform\Field\Contract\FieldRepositoryInterface;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Anomaly\Streams\Platform\Stream\Contract\StreamInterface;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;
use Defr\ExporterExtension\Field\Command\GenerateFieldsMigration;
use Defr\ExporterExtension\Stream\Command\GenerateStreamMigration;

/**
 * Controller class for admin.
 *
 * @package defr.extension.exporter
 *
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 */
class ExportsController extends AdminController
{

    /**
     * Export fields to the migration
     *
     * @param  FieldRepositoryInterface $fields    The fields
     * @param  string                   $namespace The namespace
     * @return Response
     */
    public function fields(FieldRepositoryInterface $fields, $namespace)
    {
        if ($this->request->getMethod() != 'POST')
        {
            $this->messages->error('defr.extension.exporter::message.migration_create_failed');

            return back();
        }

        /* @var FieldCollection $selected */
        $selected = $fields->findAllByNamespace($namespace)->filter(
            function (FieldInterface $field)
            {
                return in_array(
                    $field->slug,
                    array_get($this->request->all(), 'fields', [])
                );
            }
        );

        if (!$this->dispatch(new GenerateFieldsMigration($selected, $namespace)))
        {
            $this->messages->error('defr.extension.exporter::message.migration_create_failed');

            return back();
        }

        $this->messages->success('defr.extension.exporter::message.migration_created');

        return back();
    }

    /**
     * Choose which fields to add to migration
     *
     * @param  FieldRepositoryInterface $fields
     * @param  string                   $namespace The namespace
     * @return Response
     */
    public function chooseFields(FieldRepositoryInterface $fields, $namespace)
    {
        return view(
            'defr.extension.exporter::admin.fields.choose',
            [
                'fields'    => $fields->findAllByNamespace($namespace),
                'namespace' => $namespace,
            ]
        );
    }

    /**
     * Creates a stream migrations
     *
     * @param StreamRepositoryInterface $streams   The streams
     * @param string                    $namespace The namespace
     * @param $namespace
     */
    public function streams(StreamRepositoryInterface $streams, $namespace)
    {
        if ($this->request->getMethod() != 'POST')
        {
            $this->messages->error('defr.extension.exporter::message.migration_create_failed');

            return back();
        }

        /* @var StreamCollection $selected */
        $selected = $streams->findAllByNamespace($namespace)->filter(
            function (StreamInterface $stream)
            {
                return in_array(
                    $stream->slug,
                    array_get($this->request->all(), 'streams', [])
                );
            }
        )->each(
            function (StreamInterface $stream) use ($namespace)
            {
                if ($this->dispatch(new GenerateStreamMigration($stream, $namespace)))
                {
                    return;
                }

                $this->messages->error('defr.extension.exporter::message.migration_create_failed');

                return back();
            }
        );

        $this->messages->success('defr.extension.exporter::message.migration_created');

        return back();
    }

    /**
     * Show modal for choosing streams to make migrations
     *
     * @param  StreamRepositoryInterface $streams
     * @param  string                    $namespace The namespace
     * @return Response
     */
    public function chooseStreams(StreamRepositoryInterface $streams, $namespace)
    {
        return view(
            'defr.extension.exporter::admin.streams.choose',
            [
                'streams'   => $streams->findAllByNamespace($namespace),
                'namespace' => $namespace,
            ]
        );
    }
}
