<?php namespace Defr\ExporterExtension\Listener;

use Anomaly\Streams\Platform\Ui\ControlPanel\Component\Section\Event\GatherSections;

/**
 * Class for modify control panel.
 *
 * @package defr.extension.exporter
 *
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 */
class ModifyControlPanel
{

    /**
     * Handle the command
     *
     * @param GatherSections $event The event
     */
    public function handle(GatherSections $event)
    {
        $segments = app('request')->segments();

        if (array_get($segments, 0) !== 'admin')
        {
            return;
        }

        /* @var ControlPanelBuilder $builder */
        $builder = $event->getBuilder();

        $sections  = $builder->getSections();
        $namespace = array_get($segments, 1);
        $section   = array_get($segments, 2, array_get(array_reverse($segments), 0));

        if ($section == 'fields')
        {
            array_set(
                $sections,
                "{$section}.buttons",
                array_merge(
                    array_get($sections, "{$section}.buttons", []),
                    [
                        'export' => [
                            'text'        => trans('streams::button.export') . ' fields',
                            'data-toggle' => 'modal',
                            'data-target' => '#modal-large',
                            'href'        => "admin/{$namespace}/fields/export/choose",
                        ],
                    ]
                )
            );

            return $builder->setSections($sections);
        }

        array_set(
            $sections,
            "{$section}.buttons",
            array_merge(
                array_get($sections, "{$section}.buttons", []),
                [
                    'export' => [
                        'text'        => trans('streams::button.export') . ' streams',
                        'data-toggle' => 'modal',
                        'data-target' => '#modal-large',
                        'href'        => "admin/{$namespace}/streams/export/choose",
                    ],
                ]
            )
        );

        $builder->setSections($sections);
    }
}
