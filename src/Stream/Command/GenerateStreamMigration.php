<?php namespace Defr\ExporterExtension\Stream\Command;

use Anomaly\Streams\Platform\Addon\Command\GetAddon;
use Anomaly\Streams\Platform\Assignment\Contract\AssignmentInterface;
use Anomaly\Streams\Platform\Field\Contract\FieldRepositoryInterface;
use Anomaly\Streams\Platform\Stream\Contract\StreamInterface;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Bus\DispatchesJobs;

class GenerateStreamMigration
{
    use DispatchesJobs;

    /**
     * Stream object
     *
     * @var StreamInterface
     */
    protected $stream;

    /**
     * Addon namespace
     *
     * @var string
     */
    protected $namespace;

    /**
     * Create an instance of GenerateStreamMigration class
     *
     * @param StreamInterface $stream    The stream
     * @param string          $namespace The namespace
     */
    public function __construct(StreamInterface $stream, string $namespace)
    {
        $this->stream    = $stream;
        $this->namespace = $namespace;
    }

    /**
     * Handle the command
     *
     * @param  Filesystem $files The files
     * @return boolean
     */
    public function handle(Filesystem $files, FieldRepositoryInterface $fields)
    {
        $attributes  = array_except(
            $this->stream->getAttributes(),
            ['prefix', 'id']
        );

        array_set(
            $attributes,
            'config',
            unserialize(array_get($attributes, 'config'))
        );

        $stream_data = '';

        foreach ($attributes as $key => $value)
        {
            if (is_array($value))
            {
                if (!count($value))
                {
                    $stream_data .= "        '{$key}' => [],\n";
                    continue;
                }

                $stream_data .= "        '{$key}' => [\n";

                foreach ($value as $k => $v)
                {
                    if (is_array($v))
                    {
                        $stream_data .= "                '{$kk}' => '{$vv}',\n";
                        continue;
                    }

                    if (is_bool($v))
                    {
                        $converted = ($v) ? 'true' : 'false';
                        $output .= "            '{$k}' => {$converted},\n";
                        continue;
                    }

                    $stream_data .= "         '{$k}' => '{$v}',\n";
                }

                $stream_data .= "        ],\n";
                continue;
            }

            if (is_bool($value))
            {
                $converted = ($value) ? 'true' : 'false';
                $output .= "        '{$key}' => {$converted},\n";
                continue;
            }

            $stream_data .= "        '{$key}' => '{$value}',\n";
        }

        $assignments_data = $this->stream->getAssignments()->map(
            /* @var AssignmentInterface $assignment */
            function (AssignmentInterface $assignment) use ($fields)
            {
                $attributes = array_except(
                    $assignment->getAttributes(),
                    ['prefix', 'id', 'sort_order', 'stream_id', 'field_id']
                );

                array_set($attributes, 'config', unserialize(array_get($attributes, 'config')));

                /* @var FieldInterface $field */
                $field = $fields->find($assignment->getAttribute('field_id'));

                $output = "        '{$field->slug}' => [\n";

                foreach ($attributes as $key => $value)
                {
                    if (is_array($value))
                    {
                        if (!count($value))
                        {
                            $output .= "            '{$key}' => [],\n";
                            continue;
                        }

                        $output .= "            '{$key}' => [\n";

                        foreach ($value as $k => $v)
                        {
                            if (is_array($v))
                            {
                                $output .= "                    '{$kk}' => '{$vv}',\n";
                                continue;
                            }

                            if (is_bool($v))
                            {
                                $converted = ($v) ? 'true' : 'false';
                                $output .= "            '{$k}' => {$converted},\n";
                                continue;
                            }

                            $output .= "                '{$k}' => '{$v}',\n";
                        }

                        $output .= "            ],\n";
                        continue;
                    }

                    if (is_bool($value))
                    {
                        $converted = ($value) ? 'true' : 'false';
                        $output .= "            '{$key}' => {$converted},\n";
                        continue;
                    }

                    $output .= "            '{$key}' => '{$value}',\n";
                }

                $output .= "        ],\n";

                return $output;
            }
        )->all();

        $path = __DIR__ . '/../../../resources/generated';

        if (!$files->exists($path))
        {
            $files->makeDirectory($path);
        }

        /* @var Addon $addon */
        $addon = $this->dispatch(new GetAddon($this->namespace));

        $vendor = $addon->getVendor();
        $type   = $addon->getType();
        $slug   = $addon->getSlug();
        $plural = $this->stream->getSlug();

        if ($files->put(
            $path . "/{$vendor}.{$type}.{$slug}__create_{$plural}_stream.php",
            "<?php\n\n"
            . "use Anomaly\Streams\Platform\Database\Migration\Migration;\n\n"
            . 'class ' . ucfirst($vendor) . ucfirst($type) . ucfirst($slug)
            . 'Create' . ucfirst($plural) . "Stream extends Migration\n{\n\n    "
            . "/**\n     * The stream definition.\n     *\n     * @var array\n     "
            . "*/\n    protected \$stream = [\n" . $stream_data
            . "    ];\n\n    /**\n     * Assignments.\n     *\n     * @var array\n     "
            . "*/\n    protected \$assignments = [\n" . implode('', $assignments_data)
            . "    ];\n}\n"
        ))
        {
            return true;
        }

        return false;
    }
}
