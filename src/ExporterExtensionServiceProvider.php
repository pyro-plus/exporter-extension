<?php namespace Defr\ExporterExtension;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Anomaly\Streams\Platform\Ui\ControlPanel\Component\Section\Event\GatherSections;
use Defr\ExporterExtension\Listener\ModifyControlPanel;

/**
 * Service provider class.
 *
 * @package defr.extension.exporter
 *
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 */
class ExporterExtensionServiceProvider extends AddonServiceProvider
{

    /**
     * Addon routes
     *
     * @type array|null
     */
    protected $routes = [
        'admin/{namespace}/fields/export' => 'Defr\ExporterExtension\Http\Controller\Admin\ExportsController@fields',
        'admin/{namespace}/fields/export/choose' => 'Defr\ExporterExtension\Http\Controller\Admin\ExportsController@chooseFields',
        'admin/{namespace}/streams/export' => 'Defr\ExporterExtension\Http\Controller\Admin\ExportsController@streams',
        'admin/{namespace}/streams/export/choose' => 'Defr\ExporterExtension\Http\Controller\Admin\ExportsController@chooseStreams',
    ];

    /**
     * Addon listeners
     *
     * @type array|null
     */
    protected $listeners = [
        GatherSections::class => [
            ModifyControlPanel::class,
        ],
    ];
}
