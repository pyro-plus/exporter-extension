<?php namespace Defr\ExporterExtension\Field\Command;

use Anomaly\Streams\Platform\Addon\Addon;
use Anomaly\Streams\Platform\Addon\Command\GetAddon;
use Anomaly\Streams\Platform\Field\Contract\FieldInterface;
use Anomaly\Streams\Platform\Field\FieldCollection;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class for generate fields migration.
 *
 * @package defr.extension.exporter
 *
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 */
class GenerateFieldsMigration
{

    use DispatchesJobs;

    /**
     * Fields
     *
     * @var FieldCollection
     */
    protected $fields;

    /**
     * Addon namespace
     *
     * @var string
     */
    protected $namespace;

    /**
     * Create an instance of GenerateFieldsMigration class
     *
     * @param FieldCollection $addon     The addon
     * @param string          $namespace The namespace
     */
    public function __construct(FieldCollection $fields, string $namespace)
    {
        $this->fields    = $fields;
        $this->namespace = $namespace;
    }

    /**
     * Handle the command
     *
     * @param  Filesystem $files The files
     * @return boolean
     */
    public function handle(Filesystem $files)
    {
        $migration = $this->fields->map(
            function (FieldInterface $field)
            {
                $attributes = array_except($field->getAttributes(), ['slug', 'id']);

                array_set($attributes, 'config', unserialize(array_get($attributes, 'config')));

                $output = "        '{$field->getAttribute('slug')}' => [\n";

                foreach ($attributes as $key => $value)
                {
                    if (is_array($value))
                    {
                        if (!count($value))
                        {
                            $output .= "            '{$key}' => [],\n";
                            continue;
                        }

                        $output .= "            '{$key}' => [\n";
                        foreach ($value as $k => $v)
                        {
                            if (is_array($v))
                            {
                                if (!count($v))
                                {
                                    $output .= "                '{$k}' => [],\n";
                                    continue;
                                }

                                $output .= "                '{$k}' => [\n";
                                foreach ($v as $kk => $vv)
                                {
                                    if ((intval($v) !== 0 && $v !== '0')
                                        || (intval($v) === 0 && $v === '0'))
                                    {
                                        $output .= "                '{$k}' => {$v},\n";
                                        continue;
                                    }

                                    $output .= "                    '{$kk}' => '{$vv}',\n";
                                }
                                $output .= "                ],\n";
                                continue;
                            }

                            if ((intval($v) !== 0 && $v !== '0')
                                || (intval($v) === 0 && $v === '0'))
                            {
                                $output .= "                '{$k}' => {$v},\n";
                                continue;
                            }

                            $output .= "                '{$k}' => '{$v}',\n";
                        }

                        $output .= "            ],\n";
                        continue;
                    }

                    if ((intval($value) !== 0 && $value !== '0')
                        || (intval($value) === 0 && $value === '0'))
                    {
                        $output .= "            '{$key}' => {$value},\n";
                        continue;
                    }

                    $output .= "            '{$key}' => '{$value}',\n";
                }

                $output .= "        ],\n";

                return $output;
            }
        )->all();

        $path = __DIR__ . '/../../../resources/generated';

        if (!$files->exists($path))
        {
            $files->makeDirectory($path);
        }

        /* @var Addon $addon */
        $addon = $this->dispatch(new GetAddon($this->namespace));

        $vendor = $addon->getVendor();
        $type   = $addon->getType();
        $slug   = $addon->getSlug();

        if ($files->put(
            $path . "/{$vendor}.{$type}.{$slug}__create_{$slug}_fields.php",
            "<?php\n\n"
            . "use Anomaly\Streams\Platform\Database\Migration\Migration;\n\n"
            . 'class ' . ucfirst($vendor) . ucfirst($type) . ucfirst($slug)
            . 'Create' . ucfirst($slug) . "Fields extends Migration\n{\n\n    "
            . "/**\n     * The addon fields.\n     *\n     * @var array\n     "
            . "*/\n    protected \$fields = [\n" . implode('', $migration)
            . "    ];\n}\n"
        ))
        {
            return true;
        }

        return false;
    }
}
