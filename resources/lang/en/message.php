<?php

return [
    'migration_created'       => 'Migration created successfully!',
    'migration_create_failed' => 'There is an error creating migration!',
    'choose_fields'           => 'Choose fields',
    'choose_streams'          => 'Choose streams',
];
