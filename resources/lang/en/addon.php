<?php

return [
    'title'       => 'Exporter',
    'name'        => 'Exporter Extension',
    'description' => 'Creates fields migration file for addon',
];
