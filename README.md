# Exporter Extension

## PyroCMS Streams Platform Addon: `exporter-extension`

Generate migrations for fields, streams, and assignments for any addon.

## Author

Denis Efremov - [efremov.a.denis@gmail.com](mailto:efremov.a.denis@gmail.com)

## Features

- Adds **Export fields** button to each fields section
- Adds **Export streams** button to each stream section
- You can select which fields and streams you would like included in the generated migration file
- Creates ready to use migration files for fields, separately for each stream

***

## Installation

1. Copy the addon folder into the `addons/${AppReference}/defr` folder
2. Install the extension into **PyroCMS** with one of these commands: 

```bash
$ php artisan extension:install exporter
```

or

```bash
$ php artisan addon:install defr.extension.exporter
```

The "Export" buttons will now show up in each module of **PyroCMS** in the **Control Panel**.

***

## Usage

### Create a fields migration

1. Press one of new "Export" buttons
2. You will see a notification about the result of export process
3. Your generated migrations can be found inside the `extension::resources/generated` folder

***

## Examples

### Fields migration example

```php
<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class AnomalyField_typeFilesCreateFilesFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'adapter' => [
            'namespace' => 'files',
            'type' => 'anomaly.field_type.addon',
            'config' => [
                'type' => 'extensions',
                'search' => 'anomaly.module.files::adapter.*',
            ],
            'locked' => '1',
        ],
        'description' => [
            'namespace' => 'files',
            'type' => 'anomaly.field_type.textarea',
            'config' => [],
            'locked' => '1',
        ],
        'name' => [
            'namespace' => 'files',
            'type' => 'anomaly.field_type.text',
            'config' => [],
            'locked' => '1',
        ],
        'slug' => [
            'namespace' => 'files',
            'type' => 'anomaly.field_type.slug',
            'config' => [
                'slugify' => 'name',
            ],
            'locked' => '1',
        ],
    ];
}
```

### Stream migration example

```php
<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class AnomalyField_typeFilesCreateDisksStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'namespace' => 'files',
        'slug' => 'disks',
        'title_column' => 'name',
        'order_by' => 'id',
        'locked' => '0',
        'hidden' => '0',
        'sortable' => '1',
        'searchable' => '0',
        'trashable' => '1',
        'translatable' => '1',
        'config' => [],
    ];

    /**
     * Assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'config' => [],
            'unique' => '1',
            'required' => '1',
            'translatable' => '1',
        ],
        'slug' => [
            'config' => [],
            'unique' => '1',
            'required' => '1',
            'translatable' => '0',
        ],
        'adapter' => [
            'config' => [],
            'unique' => '0',
            'required' => '1',
            'translatable' => '0',
        ],
        'description' => [
            'config' => [],
            'unique' => '0',
            'required' => '0',
            'translatable' => '1',
        ],
    ];
}
```
